--[[

    Client Identifier library.
    Version 27/01/2019.

    Description:
    A Lua library to identify client(s) from UI module.


        MIT License

        Copyright (c) 2018 Anggara Yama Putra

        Permission is hereby granted, free of charge, to any person obtaining a copy
        of this software and associated documentation files (the "Software"), to deal
        in the Software without restriction, including without limitation the rights
        to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
        copies of the Software, and to permit persons to whom the Software is
        furnished to do so, subject to the following conditions:

        The above copyright notice and this permission notice shall be included in all
        copies or substantial portions of the Software.

        THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
        IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
        FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
        AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
        LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
        OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
        SOFTWARE.

]]


-- Remove unecessary table(s).
ClientID.UI = nil;


-- Local variables.
local module = ClientID.Game;
local debug = ClientID.Debug;
local syncvalue = ClientID.SyncValue;
local enum = ClientID.Enum;
local playerID = {};
local verifyID = nil;
local playerIsVerified = {};


-- SyncValue init(s).
syncvalue.Get.Instance = Game.SyncValue.Create( syncvalue.Get.NAME );
syncvalue.GetAll.Instance = Game.SyncValue.Create( syncvalue.GetAll.NAME );
syncvalue.Verify.Instance = Game.SyncValue.Create( syncvalue.Verify.NAME );
syncvalue.CurrentTime.Instance = Game.SyncValue.Create( syncvalue.CurrentTime.NAME );
if syncvalue.CurrentTime.Instance ~= nil
then
    syncvalue.CurrentTime.Instance.value = -1;
end


-- Returns the player's unique identity.
function module:GetID( index )
    return playerID[ index ];
end


-- Sets the player's unique identity.
function module:SetID( index , value )
    playerID[ index ] = value;
    debug:print( string.format( "[Game] SetID(%s)(%s)." , index , value ) );
end


-- Checks whether the player is verified.
function module:IsVerified( player )
    if player == nil then return false; end

    return playerIsVerified[ player.index ];
end


-- OnPlayerSignal hook.
function module:OnPlayerSignal( player, signal )
    local index = player.index;

    if signal == enum.SIGNAL.GET
    then
        -- Receives "GET" signal.
        debug:print( "[Game] Received ClientID.GET signal." );
        syncvalue.Get.Instance.value = self:GetID( index );
        debug:print( "[Game] Return: " .. tostring( syncvalue.Get.Instance.value ) );
    elseif signal == enum.SIGNAL.GET_ALL
    then
        -- Receives "GET_ALL" signal.
        debug:print( "[Game] Received ClientID.GET_ALL signal." );
        syncvalue.GetAll.Instance.value = tostring( table.concat(playerID,"\n") );
    elseif signal == enum.SIGNAL.VERIFY
    then
        -- Receives "VERIFY" signal.
        debug:print( "[Game] Received ClientID.VERIFY signal." );
        verifyID = self:GetID( index );
    elseif signal == enum.SIGNAL.VERIFIED
    then
        -- Receives "VERIFIED" signal.
        debug:print( "[Game] Received ClientID.VERIFIED signal." );
        playerIsVerified[index] = true;
        self:OnVerify( player, true );
    elseif signal == enum.SIGNAL.NOT_VERIFIED
    then
        -- Receives "NOT_VERIFIED" signal.
        debug:print( "[Game] Received ClientID.NOT_VERIFIED signal." );
        playerIsVerified[index] = false;
        self:OnVerify( player, false );
    else
        local newPlayerID = signal - enum.SIGNAL.OK;
        if newPlayerID > 0
        then
            -- Received "OK" signal.
            debug:print( "[Game] Received ClientID.OK signal." );
            debug:print( string.format( "[Game] (%i) => ID(%i)." , index , newPlayerID ) );

            for _,v in pairs(playerID) do
                if v == newPlayerID then
                    debug:print( "[Game] ID is exists, cancel..." );
                    return;
                end
            end

            self:SetID( index , newPlayerID );
            debug:print( string.format( "[Game] playerID(%s)." , tostring( self:GetID(index) ) ) );
        end
    end
end


-- Hookable event: OnVerify().
function module:OnVerify( player, isVerified )
end


-- OnUpdate hook.
function module:OnUpdate( time )
    -- Updates the current game time and verify id.
    syncvalue.CurrentTime.Instance.value = time;
    syncvalue.Verify.Instance.value = verifyID;
end


-- Default hook executions.
function Game.Rule:OnPlayerSignal(player, signal)
    module:OnPlayerSignal( player, signal );
end
function Game.Rule:OnUpdate(time)
    module:OnUpdate( time );
end


print( "Game.ClientIdentifier.Main is loaded!" );

