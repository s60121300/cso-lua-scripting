--[[

    Speed Record plugin.
    Version 01/03/2019.

    Description:
    A Lua addon for Speedometer plugin.
    The purpose is to tracking player's best speed records and also the best of all players speed record.


        MIT License

        Copyright (c) 2018 Anggara Yama Putra

        Permission is hereby granted, free of charge, to any person obtaining a copy
        of this software and associated documentation files (the "Software"), to deal
        in the Software without restriction, including without limitation the rights
        to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
        copies of the Software, and to permit persons to whom the Software is
        furnished to do so, subject to the following conditions:

        The above copyright notice and this permission notice shall be included in all
        copies or substantial portions of the Software.

        THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
        IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
        FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
        AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
        LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
        OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
        SOFTWARE.

]]


-- Game & UI debug mode.
SpeedoMeter.Debug.ENABLED = false;


-- Enable/disable the z-axis calculation.
SpeedoMeter.Game.IS_3D_MOVEMENT = false;


-- Best player record text label config.
SpeedoMeter.UI.Text.PlayerSpeedBest.Config = 
{
    text        = "Your best speed record: %.2f"
    , font      = "small"
    , align     = "left"
    , width     = 300
    , height    = 100
    , r         = 0
    , g         = 255
    , b         = 0
};


-- Best of al players record text label config.
SpeedoMeter.UI.Text.PlayerSpeedBestAll.Config = 
{
    text        = "The best of all players speed record:\n%s"
    , font      = "small"
    , align     = "center"
    , width     = 300
    , height    = 100
    , r         = 255
    , g         = 255
    , b         = 0
};


print( "SpeedRecord.Configuration is loaded!" );

