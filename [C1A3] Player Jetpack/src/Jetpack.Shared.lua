--[[

    Player Jetpack plugin.
    Version 01/03/2019.

    Description:
    A Lua plugin to brings a new feature to the players, a functional jetpack.


        MIT License

        Copyright (c) 2018 Anggara Yama Putra

        Permission is hereby granted, free of charge, to any person obtaining a copy
        of this software and associated documentation files (the "Software"), to deal
        in the Software without restriction, including without limitation the rights
        to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
        copies of the Software, and to permit persons to whom the Software is
        furnished to do so, subject to the following conditions:

        The above copyright notice and this permission notice shall be included in all
        copies or substantial portions of the Software.

        THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
        IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
        FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
        AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
        LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
        OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
        SOFTWARE.

]]


-- Global instance.
Jetpack =
{
    Debug = {}
    , Game =
    {
        Player = {}
    }

    , UI =
    {
        Text =
        {
            Tooltip = {}

            , Fuel =
            {
                Label = {}
                , Percentage = {}
            }

            , Status =
            {
                Label = {}
                , OnOff = {}
            }
        }

        , Box =
        {
            Fuel =
            {
                Instances = {}
            }
        }
    }

    , Vector = {}

    , SyncValue =
    {
        HasJetpack = {}
        , OnOff = {}
        , Fuel = {}
    }

    , Enum =
    {
        BUTTON = {}
    }
};


-- Prints debug message.
function Jetpack.Debug:print( msg )
    if self.Enabled
    then
        print( msg );
    end
end


Jetpack.Vector.EQUAL_EPSILON = 0.001;


function Jetpack.Vector:AngleMod( a )
    return (360.0/65536) * (math.floor(a*(65536/360.0)) & 65535);
end


function Jetpack.Vector:GetQuadrant( a )
    local angle = self:AngleMod( a );
    return math.floor( (angle/90) % 4 + 1 );
end


function Jetpack.Vector:Range90Angle( source )
    local angle = self:AngleMod( source );
    local target = 90;

    if angle > 270 then
        target = 360;
    elseif angle > 180 then
        target = 270;
    elseif angle > 90 then
        target = 180
    end

    return (target - angle) / 90;
end


-- Subtracts 2 vectors.
function Jetpack.Vector:Subtract( a, b )
    return { x = a.x - b.x , y = a.y - b.y , z = a.z - b.z };
end


function Jetpack.Vector:DotProduct( a, b )
    return ( a.x * b.x + a.y * b.y + a.z * b.z );
end


function Jetpack.Vector:CrossProduct( a, b )
    local c = {};

    c.x = a.y * b.z - a.z * b.y;
    c.y = a.z * b.x - a.x * b.z;
    c.z = a.x * b.y - a.y * b.x;

    return c;
end


function Jetpack.Vector:VectorMA( a, scale, b )
    local c = {};

    c.x = a.x + scale * b.x;
    c.y = a.y + scale * b.y;
    c.z = a.z + scale * b.z;

    return c;
end


function Jetpack.Vector:Normalize( v )
    local c = {};
    local ilength = math.sqrt( self:DotProduct(v, v) );
    
    if ilength > 0 then 
        ilength = 1.0 / ilength;
    end
        
    c.x = v.x * ilength;
    c.y = v.y * ilength;
    c.z = v.z * ilength;

    return c;
end


-- Gets length of a 2D vector.
function Jetpack.Vector:Length2D( vector )
    return math.sqrt( vector.x * vector.x + vector.y * vector.y );
end


function Jetpack.Vector:VectorVectors( forward )
    local d;
    local right = {};
    local up    = {};

    right.x = forward.z;
    right.y = -forward.x;
    right.z = forward.y;

    d     = self:DotProduct( forward, right );
    right = self:VectorMA( right, -d, forward );
    right = self:Normalize( right );
    up    = self:CrossProduct( right, forward );

    return right, up;
end


--[[ from this link: https://github.com/FWGS/xash3d/blob/master/engine/common/mathlib.c#L351 ]]
-- Converts a vector into angles.
function Jetpack.Vector:VectorAngles( forward )
    local tmp, yaw, pitch;

    if( forward.y == 0 and forward.x == 0 )
	then
		-- fast case
		yaw = 0;
        if( forward.z > 0 )
        then
			pitch = 90.0;
		else pitch = 270.0; end
	else
		yaw = ( math.atan( forward.y, forward.x ) * 180 / math.pi );
		if( yaw < 0 ) then yaw =  yaw + 360; end

		tmp = math.sqrt( forward.x * forward.x + forward.y * forward.y );
		pitch = ( math.atan( forward.z, tmp ) * 180 / math.pi );
		if( pitch < 0 ) then pitch = pitch + 360; end
    end
    
    return { x = pitch, y = yaw, z = 0 }; 
end


--[[ from this link: https://github.com/FWGS/xash3d/blob/master/engine/common/mathlib.c#L388 ]]
-- Converts vectors into angles.
function Jetpack.Vector:VectorsAngles( forward, right, up )
	local pitch, cpitch, yaw, roll;

	pitch = -math.asin( forward.z );
	cpitch = math.cos( pitch );

	if( math.abs( cpitch ) > self.EQUAL_EPSILON )	-- gimball lock?
	then
		cpitch = 1.0 / cpitch;
		pitch = math.deg( pitch );
		yaw = math.deg( math.atan( forward.y * cpitch, forward.x * cpitch ));
		roll = math.deg( math.atan( -right.z * cpitch, up.z * cpitch ));
	else
		pitch = forward.z > 0 and -90.0 or 90.0;
		yaw = math.deg( math.atan( right.x, -right.y ));
		roll = 180.0;
    end
    
    return { x = pitch, y = yaw, z = roll };
end


--[[ from this link: https://github.com/FWGS/xash3d/blob/master/engine/common/mathlib.c#L309 ]]
-- Converts angles into vectors.
function Jetpack.Vector:AngleVectors( angles )
    local forward, right, up = {}, {}, {};
    local sr, sp, sy, cr, cp, cy;

    local radian = math.rad( angles.y );
    -- yaw
    sy = math.sin( radian );
    cy = math.cos( radian );

    -- pitch
    radian = math.rad( angles.x );
    sp = math.sin( radian );
    cp = math.cos( radian );

    -- roll
    radian = math.rad( angles.z );
    sr = math.sin( radian );
    cr = math.cos( radian );

    forward.x = cp * cy;
    forward.y = cp * sy;
    forward.z = -sp;

    right.x = (-1.0 * sr * sp * cy + -1.0 * cr * -sy);
    right.y = (-1.0 * sr * sp * sy + -1.0 * cr * cy);
    right.z = (-1.0 * sr * cp);
    
    up.x = (cr * sp * cy + -sr * -sy);
    up.y = (cr * sp * sy + -sr * cy);
    up.z = (cr * cp);

    -- forward, right, up.
    return  forward, right, up;
end


-- SyncValue variable name.
Jetpack.SyncValue.HasJetpack.NAME   = "Jetpack.%i.HasJetpack";
Jetpack.SyncValue.OnOff.NAME        = "Jetpack.%i.OnOff";
Jetpack.SyncValue.Fuel.NAME         = "Jetpack.%i.Fuel";


-- Move direction.
Jetpack.Enum.DIRECTION =
{
    FORWARD     = (1 << 0)
    , BACKWARD  = (1 << 1)
    , LEFT      = (1 << 2)
    , RIGHT     = (1 << 3)
    , UPWARD    = (1 << 4)
}


-- Signal.
Jetpack.Enum.SIGNAL =
{
    MOVE_FORWARD     = 1548126674
    , MOVE_BACKWARD  = 1548126675
    , MOVE_LEFT      = 1548126676
    , MOVE_RIGHT     = 1548126677
    , MOVE_UPWARD    = 1548126678
    , TOGGLE         = 1548126679
    , LANDING       = 1548126680
}


-- Pressing button.
Jetpack.Enum.BUTTON.PRESSING =
{
    FORWARD     = (1 << 0)
    , BACKWARD  = (1 << 1)
    , LEFT      = (1 << 2)
    , RIGHT     = (1 << 3)
    , UPWARD    = (1 << 4)
    , TOGGLE    = (1 << 5)
    , LANDING  = (1 << 6)
}


print( "Jetpack.Shared is loaded!" );

