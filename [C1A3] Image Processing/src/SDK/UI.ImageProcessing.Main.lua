--[[

    Image Processing library.
    Version 17/01/2019.

    Description:
    A Lua library to processing images on client's screen.


        MIT License

        Copyright (c) 2018 Anggara Yama Putra

        Permission is hereby granted, free of charge, to any person obtaining a copy
        of this software and associated documentation files (the "Software"), to deal
        in the Software without restriction, including without limitation the rights
        to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
        copies of the Software, and to permit persons to whom the Software is
        furnished to do so, subject to the following conditions:

        The above copyright notice and this permission notice shall be included in all
        copies or substantial portions of the Software.

        THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
        IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
        FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
        AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
        LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
        OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
        SOFTWARE.

]]


-- Local variables.
Image.UI.Box.Pixel.Instances = {};
local debug = Image.Debug;
local imgPixels = Image.UI.Box.Pixel.Instances;
local width, height;


-- Checks whether current image is empty.
function Image.UI:IsEmpty()
    return #imgPixels <= 0
end


-- Gets current image width.
function Image.UI:GetWidth()
    return width;
end


-- Gets current image height.
function Image.UI:GetHeight()
    return height;
end


-- Gets total pixels.
function Image.UI:PixelCount()
    return ( width and width or 0 ) * ( height and height or 0 );
end


-- Creates an empty image with given position coordinates and image size.
function Image.UI:Create( position , size )
    if type( position ) ~= "table"
    then
        debug:print( "Image:Create() position is not a table." )
        return nil;
    end

    if type( size ) ~= "table"
    then
        debug:print( "Image:Create() size is not a table." )
        return nil;
    end

    -- Clear the old pixels.
    if Image.UI:IsEmpty() == false
    then self:Clear(); end

    width, height = size.width, size.height;
    local config = Image.UI.Box.Pixel.Config;
    local pos_x, pos_y = position.x, position.y;
    local last_x = 1;
    for i = 1, self:PixelCount()
    do
        local box = ( imgPixels[i] == nil and UI.Box.Create() or imgPixels[i] );
        if box ~= nil
        then
            box:Set({
                width  = config.width,
                height = config.height,
                r      = config.r,
                g      = config.g,
                b      = config.b,
                a      = config.a,
                x      = pos_x,
                y      = pos_y
            });

            if config.isHidden
            then
                box:Hide();
            end

            imgPixels[i] = box;
        end

        pos_x = pos_x + config.width;
        last_x = last_x + 1;
        if last_x > width
        then
            last_x = 1;
            pos_x = position.x;
            pos_y = pos_y + config.height;
        end
    end

    debug:print( string.format( "Image:Create() Success(%i).", #imgPixels ) );
    return imgPixels;
end


-- Clears the current image.
function Image.UI:Clear()
    if type( imgPixels ) ~= "table"
    then
        debug:print( "Image:Clear() pixels is not a table." )
        return;
    end

    for i = 1, self:PixelCount()
    do
        if imgPixels[i] ~= nil
        then imgPixels[i]:Hide(); end
    end

    debug:print( string.format( "Image:Clear() Success(%i).", #imgPixels ) );
end


-- Shown the current image.
function Image.UI:Show()
    if type( imgPixels ) ~= "table"
    then
        debug:print( "Image:Show() pixels is not a table." )
        return nil;
    end

    for i = 1, self:PixelCount()
    do
        if imgPixels[i] ~= nil
        then imgPixels[i]:Show(); end
    end

    debug:print( string.format( "Image:Show() Success(%i).", #imgPixels ) );
end


-- Gets a pixel from given position coordinates.
function Image.UI:Get( position )
    if type( position ) ~= "table"
    then
        debug:print( "Image:Get() position is not a table." )
        return nil;
    end

    if type( width ) ~= "number"
    then
        debug:print( "Image:Get() width is not a number." )
        return nil;
    end

    return imgPixels[ position.x + position.y * width + 1 ];
end


-- Sets the current image with pixels data.
function Image.UI:Set( data , data_type )
    if type( imgPixels ) ~= "table"
    then
        debug:print( "Image:Set() pixels is not a table." )
        return;
    end

    if data_type == nil
    then
        data_type = Image.Enum.TYPE.DEFAULT;
    end

    if type( data_type ) ~= "number"
    then
        debug:print( "Image:Set() type is not a number." )
        return;
    end

    if data == nil
    then 
        self:Clear(); 
        return;
    end

    if type( data ) ~= "table"
    then
        debug:print( "Image:Set() data is not a table." )
        return;
    end

    local result = false;
    if data_type == Image.Enum.TYPE.IMAGEMAGICK
    then
        debug:print( "Image:Set() ImageMagick mode." )
        result = Image.ImageMagick:Set( data );
    end

    debug:print( string.format( "Image:Set() %s(%i;%i).", (result and "Success" or "Failed"), #imgPixels, #data ) );
end


-- Sets the current image with ImageMagick raw pixels data.
function Image.ImageMagick:Set( data )
    if type( imgPixels ) ~= "table" or type( data ) ~= "table"
    then return false; end

    local counter = 1;
    local index = 1;
    for k,v in ipairs(data)
    do
        -- Ignores first 13 indexes.
        if k > 13
        then
            if counter % 3 == 0
            then
                if imgPixels[index] ~= nil
                then
                    imgPixels[index]:Set({
                        r = data[k-2]
                        , g = data[k-1]
                        , b = data[k]
                    });
                end
                index = index + 1;
            end
            counter = counter + 1;
        end

        if index > #imgPixels
        then
            debug:print( string.format( "Image:Set() Limit reached(%i;%i).", index , #imgPixels ) );
            break;
        end
    end

    return true;
end


print( "UI.ImageProcessing.Main is loaded!" );

