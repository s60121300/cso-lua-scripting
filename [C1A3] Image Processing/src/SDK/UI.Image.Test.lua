--[[

    Image Processing library.
    Version 17/01/2019.

    Description:
    A Lua library to processing images on client's screen.


        MIT License

        Copyright (c) 2018 Anggara Yama Putra

        Permission is hereby granted, free of charge, to any person obtaining a copy
        of this software and associated documentation files (the "Software"), to deal
        in the Software without restriction, including without limitation the rights
        to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
        copies of the Software, and to permit persons to whom the Software is
        furnished to do so, subject to the following conditions:

        The above copyright notice and this permission notice shall be included in all
        copies or substantial portions of the Software.

        THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
        IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
        FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
        AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
        LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
        OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
        SOFTWARE.

]]


--[[
    Convert your image into pixels array via this web service :
            https://imagemagick.online/
    Sets the output file extension with `.h`
    
    Optional, compress your image Lua script with this web service :
            https://mothereff.in/lua-minifier
--]]


function UI.Event:OnChat(msg)
    if string.lower(msg) == "where is hl3?"
    then
        Image.UI:Create( { x = 0 , y = 0 } , { width = 32, height = 28 } );
        Image.UI:Set( img_hl3_logo );
        Image.UI:Show();
    end

    if string.lower(msg) == "man of few words"
    then
        Image.UI:Create( { x = 0 , y = 0 } , { width = 26, height = 32 } );
        Image.UI:Set( img_gordonfreeman );
        Image.UI:Show();
    end

    if string.lower(msg) == "ralsei"
    then
        Image.UI:Create( { x = UI.ScreenSize().width/2 , y = 0 } , { width = 17, height = 32 } );
        Image.UI:Set( img_ralsei );
        Image.UI:Show();
    end

    if string.lower(msg) == "douglas"
    then
        Image.UI:Create( { x = 0 , y = UI.ScreenSize().height/2 } , { width = 32, height = 28 } );
        Image.UI:Set( img_douglasjacob );
        Image.UI:Show();
    end

    if string.lower(msg) == "imgclear"
    then
        Image.UI:Clear();
    end

    if string.lower(msg) == "imgdel"
    then
        if Image.UI:IsEmpty()
        then return; end

        local pos = { x = math.random(0,Image.UI:GetWidth()) , y = math.random(0,Image.UI:GetHeight()) };
        local pixel = Image.UI:Get( pos );
        if pixel ~= nil
        then
            pixel:Hide();
        end
    end
end



print( "UI.Image.Test is loaded!" );

