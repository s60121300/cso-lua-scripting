--[[

    Image Processing library.
    Version 17/01/2019.

    Description:
    A Lua library to processing images on client's screen.


        MIT License

        Copyright (c) 2018 Anggara Yama Putra

        Permission is hereby granted, free of charge, to any person obtaining a copy
        of this software and associated documentation files (the "Software"), to deal
        in the Software without restriction, including without limitation the rights
        to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
        copies of the Software, and to permit persons to whom the Software is
        furnished to do so, subject to the following conditions:

        The above copyright notice and this permission notice shall be included in all
        copies or substantial portions of the Software.

        THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
        IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
        FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
        AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
        LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
        OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
        SOFTWARE.

]]


-- Global instance.
Image =
{
    Debug = {}

    , UI =
    {
        Box =
        {
            Pixel = {}
        }
    }
    
    , Enum =
    {
        TYPE = {}
    }

    , ImageMagick = {}
};


-- Prints debug message.
function Image.Debug:print( msg )
    if self.Enabled
    then
        print( msg );
    end
end


-- Pixels array type.
Image.Enum.TYPE =
{
    IMAGEMAGICK   =   1


    , UNKNOWN       =   999
};
Image.Enum.TYPE.DEFAULT  = Image.Enum.TYPE.IMAGEMAGICK;


print( "ImageProcessing.Shared is loaded!" );

