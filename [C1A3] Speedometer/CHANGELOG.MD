# Changelog(s)

* 01/03/2019
  * Removed the Client Identifier library.
  * Use PlayerIndex() instead of Client Identifier library.

* 26/01/2019
  * Updated the Client Identifier library.
  * Use ClientID:OnVerify() hook for syncvar creations.
  * Removed SyncVar CurrentTime OnSync hook.

* 10/01/2019
  * Now following default CSO Lua namespace formats.
  * Configuration is separated from shared script.
  * Updated the Client Identifier library.
  * New configuration variable: Toggle switch for including z-axis movement into speed calculation.

* 28/12/2018
  * Initial public release.

