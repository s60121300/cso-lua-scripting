--[[

    Speedometer plugin.
    Version 01/03/2019.

    Description:
    A Lua plugin for displaying player's movement speed.


        MIT License

        Copyright (c) 2018 Anggara Yama Putra

        Permission is hereby granted, free of charge, to any person obtaining a copy
        of this software and associated documentation files (the "Software"), to deal
        in the Software without restriction, including without limitation the rights
        to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
        copies of the Software, and to permit persons to whom the Software is
        furnished to do so, subject to the following conditions:

        The above copyright notice and this permission notice shall be included in all
        copies or substantial portions of the Software.

        THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
        IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
        FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
        AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
        LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
        OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
        SOFTWARE.

]]


-- Remove unecessary table(s).
SpeedoMeter.Game = nil;


-- Local variables.
local module = SpeedoMeter.UI;
local txt = SpeedoMeter.UI.Text;
local debug = SpeedoMeter.Debug;
local syncvalue = SpeedoMeter.SyncValue;
local playerSpeed = -1;


-- UI element init(s).
txt.PlayerSpeed.Config.format = txt.PlayerSpeed.Config.text;
txt.PlayerSpeed.Instance = UI.Text.Create();
txt.PlayerSpeed.Instance:Set( txt.PlayerSpeed.Config );
txt.PlayerSpeed.Instance:Set({ x = UI.ScreenSize().width/2 - 150 , y = 0 });
txt.PlayerSpeed.Instance:Hide();


-- Creates speed sync variable.
syncvalue.PlayerSpeed.Instance = UI.SyncValue.Create( string.format( syncvalue.PlayerSpeed.NAME , UI.PlayerIndex() ) );
if syncvalue.PlayerSpeed.Instance ~= nil
then
    debug:print( string.format("[UI] %s is created." , syncvalue.PlayerSpeed.Instance.name) );

    function syncvalue.PlayerSpeed.Instance:OnSync()
        local speedValue = self.value;
        playerSpeed = speedValue == nil and -1 or speedValue;
        module:UpdateHUD();
    end
end


-- Returns the player's speed.
function module:GetSpeed()
    return playerSpeed;
end


-- Updates the player's speed HUD.
function module:UpdateHUD()
    local value = self:GetSpeed();
    if value == nil then value = -1 end

    txt.PlayerSpeed.Instance:Set({ text = string.format( txt.PlayerSpeed.Config.format , value ) });
    txt.PlayerSpeed.Instance:Show();
end


print( "UI.Speedometer.Main is loaded!" );

