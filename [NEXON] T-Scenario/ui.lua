
screen = UI.ScreenSize()
center = {x = screen.width / 2, y = screen.height / 2}

--------------------------------------------------------------------------------------------------
--[[ Displays Inventory Key ]]
--------------------------------------------------------------------------------------------------

weaponInvenKeyText = UI.Text.Create()
weaponInvenKeyText:Set({text = 'Inventory: [B]', font='small', align='right', x = center.x + 80, y = screen.height - 25, width = 185, height = 30, r = 255, g = 200, b = 0, a = 100})

--------------------------------------------------------------------------------------------------
--[[ Reloading UI ]]
--------------------------------------------------------------------------------------------------

reloadingInit = false
reloadingBG = UI.Box.Create()
reloadingGauge = UI.Box.Create()
reloadingText = UI.Text.Create()
reloadingBG:Hide()
reloadingGauge:Hide()
reloadingText:Hide()

reloadTime = 0 -- Amount of time required for a reload
reloadStartTime = 0 -- Time when the reload was initiated

reloadTimeSync = UI.SyncValue.Create("reloadTime")
function reloadTimeSync:OnSync()
	reloadTime = self.value
end

function UI.Event:OnUpdate(time)
	if reloadingBG:IsVisible() and reloadTime > EPSILON then
		if reloadStartTime < EPSILON then
			reloadStartTime = time
		else
			-- Calculates the time passed since the reload was initiated to change the gauge
			local diff = time - reloadStartTime
			local ratio = diff / reloadTime
			if ratio > 1 then
				reloadingBG:Hide()
				reloadingGauge:Hide()
				reloadingText:Hide()
				reloadingGauge:Set({width = 0})
				reloadTime = 0
			else
				reloadingGauge:Set({width = 150 * ratio})
			end
		end
	end
end

--------------------------------------------------------------------------------------------------
--[[ Event Functions ]]
--------------------------------------------------------------------------------------------------

-- Event function for when a key is pressed
function UI.Event:OnKeyDown(inputs)
	if inputs[UI.KEY.B] then
		UI.Signal(SignalToGame.openWeaponInven)
	end
end

-- Delivering data between the game
function UI.Event:OnSignal(signal)
	if signal == SignalToUI.reloadStarted then
		
		-- Fills the data upon initial open
		if reloadingInit == false then
			reloadingInit = true
			reloadingBG:Set({x = center.x - 75, y = center.y - 20, width = 150, height = 27, r = 25, g = 25, b = 25, a = 220})
			reloadingGauge:Set({x = center.x - 75, y = center.y - 20, width = 150, height = 27, r = 100, g = 120, b = 150, a = 220})
			reloadingText:Set({text = 'Getting Ready', font='small', align='center', x = center.x - 75, y = center.y - 4, width = 150, height = 20, r = 255, g = 255, b = 255})
		end
		
		reloadingBG:Show()
		reloadingGauge:Show()
		reloadingText:Show()
		reloadingGauge:Set({width = 0})
		reloadStartTime = 0
		reloadTime = 0

	elseif signal == SignalToUI.reloadFinished then
		reloadingBG:Hide()
		reloadingGauge:Hide()
		reloadingText:Hide()
		reloadingGauge:Set({width = 0})
		reloadTime = 0
	end
end
