# Changelog(s)

* 04/20/2019.
  * All non-fademode screen fade parameters are now accept float values.

* 09/04/2019.
  * Initial public release.

* 05/03/2019
  * Initial closed beta release.

