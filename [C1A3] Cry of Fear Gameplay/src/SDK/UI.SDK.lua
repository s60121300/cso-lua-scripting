-- Map-exclusive game script file.

COF.UI.Text.Prologue.Config.text = [[
INTRODUCTION


Hello, this is AnggaraNothing speaking, and
I'd just like to say that this is an example campaign, 
of how you can set up your own custom campaign.

To make your own campaigns, copy this
sample map from studio map browser.]];


COF.UI.Text.Credit.Config.text = [[
#CRY OF FEAR SDK
04/20/2019


#SDK Creator:

Anggara "AnggaraNothing" Yama Putra


#Creators of original Cry of Fear SDK:

Andreas "ruMpel" Ronnberg

James "Minuit" Marchant





#THANKS FOR PLAYING!]];


COF.Subtitle.GAME_SAVED =
{
    text        = "New spawnpoint is available."
    , holdtime  = 5
    , r         = 0
    , g         = 255
    , b         = 0
};

