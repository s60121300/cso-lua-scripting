--[[

    Cry of Fear Gameplay.
    Version 04/20/2019.

    Description:
    Cry of Fear gameplay system for CSO Studio.


        MIT License

        Copyright (c) 2019 Anggara Yama Putra

        Permission is hereby granted, free of charge, to any person obtaining a copy
        of this software and associated documentation files (the "Software"), to deal
        in the Software without restriction, including without limitation the rights
        to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
        copies of the Software, and to permit persons to whom the Software is
        furnished to do so, subject to the following conditions:

        The above copyright notice and this permission notice shall be included in all
        copies or substantial portions of the Software.

        THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
        IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
        FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
        AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
        LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
        OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
        SOFTWARE.

]]


--[[
    WARNING : DO NOT MODIFY THIS FILE.
    I REPEAT, DO NOT MODIFY THIS FILE.

    IF YOU PROCEED ANYWAY,
    DO IT AT YOUR OWN RISK !!!
]]


-- Global instance.
COF =
{
    Debug = {}
    , Game = 
    {
        Player = {}

        , Event = {}
    }
    , UI = 
    {
        Box =
        {
            ScreenFade = {}
            , Phone = 
            {
                Screen = {}
                , Border = {}
                , Overlay = {}
            }
            , Health = 
            {
                Border = {}
                , Background = {}
                , Current = {}
                , Limit = {}
            }
            , Stamina = 
            {
                Border = {}
                , Background = {}
                , Current = {}
            }
            , Cutscene = {}
            , Hint = {}
            , Chapter = {}
        }
        , Text =
        {
            Phone = 
            {
                Subject = {}
                , Message = {}
                , Options = {}
                , Back = {}
                , NewMessage = {}
            }
            , Icon = 
            {
                Health = {}
                , Stamina = {}
            }
            , Tooltip =
            {
                Phone = {}
                , Objective = {}
            }
            , Subtitle = {}
            , Hint = {}
            , Chapter = {}
            , Prologue = {}
            , Epilogue = {}
            , Credit = {}
        }
        , ScreenFade = {}
        , Chapter = {}
        , Phone = {}
        , Health = {}
        , Stamina = {}
        , Cutscene = {}
        , Subtitle = {}
        , Hint = {}
        , Objective = {}
        , Prologue = {}
        , Epilogue = {}
        , Credit = {}

        , Event = {}
    }
    , SyncValue =
    {
        ScreenFade = {}
        , Health = {}
        , MaxHealth = {}
        , Stamina = {}
        , Phone = 
        {
            Available = {}
            , Message = {}
        }
        , Cutscene = {}
        , Subtitle = {}
        , Hint = {}
        , Objective = {}
        , Chapter = {}
        , Prologue = {}
        , Epilogue = {}
        , Credit = {}
    }

    , Vector = {}
    , Enum = {}

    , PhoneMsg = {}
    , Hint = {}
    , Subtitle = {}
};


-- Debug mode.
COF.Debug.ENABLED = true;


-- SyncValue variable name.
COF.SyncValue.ScreenFade.NAME       = "COF.ScreenFade.%i";
COF.SyncValue.Health.NAME           = "COF.Health.%i";
COF.SyncValue.MaxHealth.NAME        = "COF.MaxHealth.%i";
COF.SyncValue.Stamina.NAME          = "COF.Stamina.%i";
COF.SyncValue.Cutscene.NAME         = "COF.Cutscene.%i";
COF.SyncValue.Subtitle.NAME         = "COF.Subtitle.%i";
COF.SyncValue.Hint.NAME             = "COF.Hint.%i";
COF.SyncValue.Prologue.NAME         = "COF.Prologue.%i";
COF.SyncValue.Epilogue.NAME         = "COF.Epilogue.%i";
COF.SyncValue.Credit.NAME           = "COF.Credit.%i";
COF.SyncValue.Chapter.NAME          = "COF.Chapter";
COF.SyncValue.Phone.Available.NAME  = "COF.Phone.Available";
COF.SyncValue.Phone.Message.NAME    = "COF.Phone.Message";
COF.SyncValue.Objective.NAME        = "COF.Objective";


-- Signals.
COF.Enum.SIGNAL = 
{
    HEARTHBEAT = 1553325259
    , SPRINT = 1553325260
    , KEY_CTRL  = 1553325261
    , KEY_SHIFT = 1553325262
    , KEY_W = 1553325263
    , KEY_S = 1553325264
    , KEY_A = 1553325265
    , KEY_D = 1553325266
    , KEY_SPACE = 1553325267

    , CUTSCENE_ON = 1553325268
    , CUTSCENE_OFF = 1553325269

    , PROLOGUE_ON = 1553325270
    , PROLOGUE_OFF = 1553325271

    , EPILOGUE_ON = 1553325272
    , EPILOGUE_OFF = 1553325273

    , CREDIT_ON = 1553325274
    , CREDIT_OFF = 1553325275
};


-- Button bitflags.
COF.Enum.BUTTON =
{
    SPACE   = (1 << 1)
    , CTRL  = (1 << 2)
    , W     = (1 << 3)
    , S     = (1 << 4)
    , A     = (1 << 9)
    , D     = (1 << 10)
    , SHIFT = (1 << 12)
}


-- ScreenFade fade mode.
COF.Enum.FADEMODE =
{
    IN = 0
    , OUT = 1
    , SET = 2
}


-- Chapter SyncValue value separator.
COF.SyncValue.Chapter.Separator = "\a";


-- Player-related constants.
COF.CONNECTED_TIME_OUT = 30;
COF.MAX_PLAYER = 32;
COF.MAX_HEALTH_COMMON = 100;
COF.MAX_SPEED = 250;
COF.WALK_SPEED = 130;
COF.CROUCH_SPEED = 85.3;
COF.FASTEST_RECEIVED_DOUBLETAP_INPUT_TIME = 70;
COF.JUMP_VELOCITY_TOLERANCE_MIN = 180;
COF.JUMP_VELOCITY_TOLERANCE_MAX = 260;


-- Stamina-related constants.
COF.STAMINA_MAX = 100;


-- HUD-related constants.
COF.HUD_PADDING = 16;
COF.HUD_ICON_CROSS_WIDTH = 24;
COF.HUD_ICON_CROSS_HEIGHT = 24;
COF.HUD_ICON_SUIT_WIDTH = 24;
COF.HUD_ICON_SUIT_HEIGHT = 24;
COF.HUD_ICON_NUMERIC_WIDTH = 20;
COF.HUD_ICON_NUMERIC_HEIGHT = 25;
COF.TEXT_SMALL_WIDTH = 10;
COF.TEXT_SMALL_HEIGHT = 16;
COF.TEXT_MEDIUM_WIDTH  = 20;
COF.TEXT_MEDIUM_HEIGHT = 40;


function COF.Vector:AngleMod( a )
    return (360.0/65536) * (math.floor(a*(65536/360.0)) & 65535);
end


function COF.Vector:GetQuadrant( a )
    local angle = self:AngleMod( a );
    return math.floor( (angle/90) % 4 + 1 );
end


function COF.Vector:Range90Angle( source )
    local angle = self:AngleMod( source );
    local target = 90;

    if angle > 270 then
        target = 360;
    elseif angle > 180 then
        target = 270;
    elseif angle > 90 then
        target = 180
    end

    return (target - angle) / 90;
end


--[[ from this link: https://github.com/FWGS/xash3d/blob/master/engine/common/mathlib.c#L351 ]]
-- Converts a vector into angles.
function COF.Vector:VectorAngles( forward )
    local tmp, yaw, pitch;

    if( forward.y == 0 and forward.x == 0 )
	then
		-- fast case
		yaw = 0;
        if( forward.z > 0 )
        then
			pitch = 90.0;
		else pitch = 270.0; end
	else
		yaw = ( math.atan( forward.y, forward.x ) * 180 / math.pi );
		if( yaw < 0 ) then yaw =  yaw + 360; end

		tmp = math.sqrt( forward.x * forward.x + forward.y * forward.y );
		pitch = ( math.atan( forward.z, tmp ) * 180 / math.pi );
		if( pitch < 0 ) then pitch = pitch + 360; end
    end
    
    return { x = pitch, y = yaw, z = 0 }; 
end


-- Gets proper text dimensions.
function COF.UI.Text:GetSize( fontType )
    if fontType == 'medium'
    then
        return COF.TEXT_MEDIUM_WIDTH , COF.TEXT_MEDIUM_HEIGHT;
    
    -- Default size.
    else
        return COF.TEXT_SMALL_WIDTH , COF.TEXT_SMALL_HEIGHT;
    end
end


--[[ from this link: http://lua-users.org/wiki/SplitJoin ]]
-- Explodes a string into a table.
function string.explode( source , delimiter )
    local t, ll
    t={}
    ll=0
    if(#source == 1) then
        return {source}
    end
    while true do
        l = string.find(source, delimiter, ll, true) -- find the next d in the string
        if l ~= nil then -- if "not not" found then..
            table.insert(t, string.sub(source,ll,l-1)) -- Save it in our array.
            ll = l + 1 -- save just after where we found it for searching next time.
        else
            table.insert(t, string.sub(source,ll)) -- Save what's left in our array.
            break -- Break at end, as it should be, according to the lua manual.
        end
    end
    return t
end


-- Prints debug msg.
function COF.Debug:print( msg )
    if self.ENABLED
    then
        print( msg );
    end
end


print( "COF.Shared is loaded!" );

