@echo off

echo Removing old docs...
rmdir /S /Q "../docs"

echo.
echo Generating docs...
lua5.1 "../../.tools/LDoc-multiplemod/ldoc.lua" .

echo.
echo Finished!
pause
