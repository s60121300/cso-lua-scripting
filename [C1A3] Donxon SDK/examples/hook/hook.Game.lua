-- Assigns 2 hook functions as usually do.
function Game.Rule:OnTakeDamage ( victim, attacker, damage, weapontype, hitbox )
  print( "[a] ", victim.index );
end
function Game.Rule:OnTakeDamage ( victim, attacker, damage, weapontype, hitbox )
  print( "[b] ", type( attacker ) );
  if attacker ~= nil then
    print( "[b] ", attacker.index );
  end
end


-- Removes all registered hooks.
Hook:RemoveAll( Game.Rule.OnTakeDamage );


-- Assigns 2 another hooks with reference.
local function c ( self, victim, attacker, damage, weapontype, hitbox )
  print( "[c]old ", damage );
  damage = damage // 2;
  print( "[c]new ", damage );
  return damage;
end
local function d ( self, victim, attacker, damage, weapontype, hitbox )
  print( "[d] ", hitbox );
end
Game.Rule.OnTakeDamage = c;
Game.Rule.OnTakeDamage = d;


-- Function `d` is not executed because the previous link (`c`) was returns a value.
-- You can also returns `Hook.RETURN.HANDLED` to halts remaining executions without returning any values (nothing),
-- Or returns `Hook.RETURN.CONTINUE` to continues remaining executions without returning any values (nothing).


-- Removes an individual hook.
Hook:Remove( Game.Rule.OnTakeDamage, d );
