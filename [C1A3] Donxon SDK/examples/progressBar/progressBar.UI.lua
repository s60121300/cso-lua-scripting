-- Creates a progress bar.
bar = UI.ProgressBar:Create();


-- Sets its options.
bar:Set({
    x = UI:GetCenterPosition().x
    , y = UI:GetCenterPosition().y
    , width = 256
    , height = 64
    , r = 0
    , g = 0
    , b = 0
    , a = 153
    , stroke =
    {
        visible = true,
        size = 8,
        r = Common.COLOR.REDISH.r,
        g = Common.COLOR.REDISH.g,
        b = Common.COLOR.REDISH.b,
        a = 255,
    }
    , bar =
    {
        percent = math.random(),
        r = Common.COLOR.GREENISH.r,
        g = Common.COLOR.GREENISH.g,
        b = Common.COLOR.GREENISH.b,
        a = 255,
    }
});
-- Re-center its position.
bar:Set({
    x = UI:GetCenterPosition( bar:Get() ).x
    , y = UI:GetCenterPosition( bar:Get() ).y
});


-- Shows it.
bar:Show();


-- Is it visible?
print( "bar:IsVisible() => ", bar:IsVisible() );
