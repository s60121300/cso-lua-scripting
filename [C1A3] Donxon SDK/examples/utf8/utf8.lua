local utf8Strings = {
    "Hello World",
    "안녕하세요 월드",
    "Привет мир",
    "こんにちは世界",
    "你好，世界"
};


-- Prints every single characters and its integer code of each strings.
for _, v in ipairs(utf8Strings) do
    print('--------------------');
    for _, code in System.UTF8.codes(v) do
        -- Converts `code` back into an UTF-8 character.
        print( System.UTF8.char(code), ':', code );
    end
end