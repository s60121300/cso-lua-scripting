local MAX_VALUE = 255; -- Maximum value to reach for.
local DURATION  = 3;   -- How long until value is reached maximum vaue in second(s).


local start, current = Game.GetTime(),0;
function Game.Rule:OnUpdate( time )
    local delta = Game.GetDeltaTime();


    -- Prints delta in console.
    print( "Delta: ", delta );


    -- Increments until reaching maximum value.
    if current < MAX_VALUE then
        local addValue = MAX_VALUE / DURATION;
        current = current + ( addValue * delta );
    end


    -- Reached.
    if current >= MAX_VALUE then
        -- Prints finish time, it should be same as `DURATION`.
        print( "Finish time: " .. tostring( (time - start) ) );

        -- Restart.
        start = time;
        current = 0;
    end
end

