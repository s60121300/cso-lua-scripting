-- Shows the screen fade-in to all players.
Game.ScreenFade:Show( {
    mode = Common.ScreenFade.FFADE.IN,
    modulate = false,
    duration = 5,
    holdTime = 10,
    r = 0,
    g = 0,
    b = 0,
} );


-- Shows the screen fade-out to all players.
Game.ScreenFade:Show( {
    mode = Common.ScreenFade.FFADE.OUT,
    modulate = false,
    duration = 5,
    holdTime = 10,
    r = 0,
    g = 0,
    b = 0,
} );


-- Shows the screen fade-stay to a player.
Game.ScreenFade:Show( {
    player = Game.Player.Create(1),
    mode = Common.ScreenFade.FFADE.STAYOUT,
    modulate = false,
    r = 0,
    g = 0,
    b = 0,
    a = 128,
} );


-- Hides all player's screen fade.
-- Game.ScreenFade:Hide();
