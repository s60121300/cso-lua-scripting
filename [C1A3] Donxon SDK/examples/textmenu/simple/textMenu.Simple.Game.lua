-- Creates a menu.
local customMenu = Game.TextMenu:Create( "Your Custom Menu" )
                    :AddItem( { text = "Hello World" } )
                    :AddItem( { text = "안녕하세요 월드" } )
                    :AddItem( { text = "Привет мир" } )
                    :AddBlank()
                    :AddItem( { text = "こんにちは世界" } )
                    :AddItem( { text = "你好，世界" } );


-- Sets the menu callback.
-- player   -> The player who accessing this menu.
-- slot     -> The selected item slot. | The menu status code.
-- page     -> The menu page number.
-- item     -> The menu item property.
function customMenu:OnSlotSelected ( player, slot, page, item )
    print( "----------------" );
    print( "Menu #1" );
    print( "Name: ", player.name );
    print( "Slot: ", slot );
    print( "Page: ", page );
    print( "Item: ", (item and item.text or "<nothing>") );
    print( "----------------" );
end


-- Opens the menu for all players.
customMenu:Open();


-- Or only for first player.
-- customMenu:Open( Game.Player.Create(1) );
