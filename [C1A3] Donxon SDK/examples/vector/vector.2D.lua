-- Normal.
local a = Common.Vector2D:Create( 10 , 20 );
print( a:ToString() ); -- "10 20"


-- Table.
a = Common.Vector2D:Create( {x = 10 , y = 20} );
print( a:ToString() ); -- "10 20"


-- String.
a = Common.Vector2D:Create( "10 20" );
print( a:ToString() ); -- "10 20"


-- operands.
a = a + {x = 20}; print( a:ToString() ); -- "30 20"
a = a + {y = 10}; print( a:ToString() ); -- "30 30"
a = a - {x = 20, y = 10}; print( a:ToString() ); -- "10 20"
a = a / 2; print( a:ToString() ); -- "5 10"
a = a * 2; print( a:ToString() ); -- "10 20"


-- functions.
print( a:Length() ); -- "22.3607"
print( a:Make3D():ToString() ); -- "10 20 0"